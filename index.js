import data from './example.json' assert {type: 'json'};

const metaData = Object.entries(data.meta);
const props = data.props;

const form = $(".form");
const setDefaultsButton = $(".setDefaultsButton");
const resetButton = $(".resetButton");
const saveButton = $(".saveButton");
const dialog = $(".dialog");

const createDropdown = (name, defaultValue, values, label) => {
  const dropdown = values.map(v => {
    const value = Object.entries(v)[0];

    return `<option value="${value[0]}" ${value[0] === defaultValue?'selected':''}>${value[1]}</option>`
  });

  return `<label for="${name}">${label}</label>
          <select id="${name}">
          ${dropdown.join('')}
          </select>
         `
}

const createCheckbox = (name, defaultValue, label) => {
  return `<input type="checkbox" id="${name}" name="${name}" value="isChecked" ${defaultValue === "true"?"checked":""}
          <label for="${name}">${label}</label><br>
         `
}

const createRadio = (name, defaultValue, values, label) => {
  const radio = values.map((v, el) => {
    const value = Object.entries(v)[0];

    return `<input type="radio" id="${name + el}" name="${name}" value="${value[0]}" ${value[0] === defaultValue?'checked':''}>
            <label for="${name + el}">${value[1]}</label><br>
           `
  });

  return `<div id="${name}">
          ${radio.join('')}
          </div>
         `
}

const createInputField = (name, defaultValue, label) => {
  return `<label for="${name}">${label}</label>
          <input type="text" id="${name}" name="${name} value=${defaultValue}">
         `
}

const createForm = () => {
  const formData = metaData.map(el => {
    const description = `<h3>${el[1].description}</h3>`;
    let content = '';
    if (el[1].type === 'dropdown') {
      content = createDropdown(el[0], el[1].defaultValue, el[1].values, el[1].label)
    } else if (el[1].type === 'checkbox') {
      content = createCheckbox(el[0], el[1].defaultValue, el[1].label)
    } else if (el[1].type === 'radio') {
      content = createRadio(el[0], el[1].defaultValue, el[1].values, el[1].label)
    } else {
      content = createInputField(el[0], el[1].defaultValue, el[1].label)
    }

    return description.concat(content);
  })

  return formData;
}

form.append(createForm());

const handleResets = (resetType) => {
  metaData.map(el => {
    const resetValue = resetType === 'defaults'?el[1].defaultValue:props[el[0]];
    const elementId = "#" + el[0];
    const elementType = el[1].type;

    if (elementType === 'checkbox') {
      $(elementId).prop("checked", resetValue === 'false'?false:true);
    } else if (elementType === 'radio') {
      const optionsCount = $(elementId).children(":radio").length;

      for (let i = 0; i < optionsCount; i++) {
        const input = $(elementId + i);

        input.prop("checked", input.val() === resetValue);
      }
    } else {
      $(elementId).val(resetValue);
    }
  });
}

const handleSave = () => {
  const dialogContent = JSON.parse(JSON.stringify(data));
  console.log(dialogContent);

  metaData.map(el => {
    const name = el[0];
    const elementId = "#" + el[0];
    const elementType = el[1].type;
    console.log(props);

    if (elementType === 'checkbox') {
      dialogContent.props[name] = $(elementId).prop("checked");
    } else if (elementType === 'radio') {
      const optionsCount = $(elementId).children(":radio").length;

      for (let i = 0; i < optionsCount; i++) {
        const input = $(elementId + i);

        if (input.prop("checked")) dialogContent.props[name] = input.val();
      }
    } else {
      dialogContent.props[name] = $(elementId).val();
    }
  })

  dialog.text(JSON.stringify(dialogContent));
  dialog.prop("open", true);
  console.log(props);
}

setDefaultsButton.click(() => handleResets('defaults'));
resetButton.click(() => handleResets('reset'));
saveButton.click(() => handleSave());
